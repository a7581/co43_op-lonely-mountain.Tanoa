	force ace_medical_treatment_locationSurgicalKit = 3	// Nähen in Vehicles und Facilities
	force ace_medical_treatment_locationPAK = 2	// PAK nur in Facilities
	force ace_medical_treatment_medicEpinephrine = 1 // Epi nur ab Sani
	force ace_medical_treatment_medicSurgicalKit = 2 // Nähen nur Doktoren
	
	force ace_medical_statemachine_cardiacArrestTime = 1200	// Entspricht Server Standard, aber soll hier konkretisiert werden
	
	force acex_headless_enabled = true
	force acex_headless_delay = 15
	force acex_headless_endMission = 2
	force acex_headless_log = false