// Tanoa Sugar Company
_sugar1 = [getMarkerPos "sugar1", east, ["O_Soldier_TL_F","O_Soldier_F","O_Soldier_AR_F","O_Soldier_LAT_F"]] call BIS_fnc_spawnGroup;
[_sugar1, objNull] spawn epsilon_setSkillAndHe;
[_sugar1, getmarkerpos "sugar1", 100, [200, 100, 31.163, true, 10], true] call lambs_wp_fnc_taskGarrison;

_sugar2 = [getMarkerPos "sugar2", east, ["O_Soldier_TL_F","O_Soldier_F","O_Soldier_AR_F"]] call BIS_fnc_spawnGroup;
[_sugar2, objNull] spawn epsilon_setSkillAndHe;
[_sugar2, getmarkerpos "sugar2", 100, [200, 100, 31.163, true, 10], true] call lambs_wp_fnc_taskGarrison;

_sugar3 = [getMarkerPos "sugar3", east, ["O_Soldier_TL_F","O_Soldier_LAT_F"]] call BIS_fnc_spawnGroup;
[_sugar3, objNull] spawn epsilon_setSkillAndHe;
_sugar3 setVariable ["lambs_danger_enableGroupReinforce", true, true];
[_sugar3, getmarkerpos "sugar3", 200, 4, [350, 350, 0, false, -1], true] call lambs_wp_fnc_taskPatrol;

_sugar4 = [getMarkerPos "sugar4", east, ["O_Soldier_TL_F","O_Soldier_F","O_Soldier_AR_F","O_Soldier_LAT_F"]] call BIS_fnc_spawnGroup;
[_sugar4, objNull] spawn epsilon_setSkillAndHe;
_sugar4 setVariable ["lambs_danger_enableGroupReinforce", true, true];
[_sugar4, getmarkerpos "sugar4", 200, 4, [300, 100, 34.919, false, -1], true] call lambs_wp_fnc_taskPatrol;

_sugar5 = [getMarkerPos "sugar5", east, ["O_Soldier_TL_F","O_Soldier_F","O_Soldier_AR_F","O_Soldier_LAT_F"]] call BIS_fnc_spawnGroup;
[_sugar5, objNull] spawn epsilon_setSkillAndHe;
_sugar5 setVariable ["lambs_danger_enableGroupReinforce", true, true];
[_sugar5, getmarkerpos "sugar5", 200, 4, [300, 150, 45, false, -1], true] call lambs_wp_fnc_taskPatrol;

for "_i" from 1 to 2 do {
	clz = ["O_Soldier_TL_F"];
	clz pushBack (selectRandom ["O_Soldier_F", "O_Soldier_F", "O_Soldier_F", "O_Soldier_F", "O_Soldier_AR_F", "O_Soldier_AR_F", "O_Soldier_GL_F", "O_Soldier_LAT_F"]);
	_sugar6 = [getMarkerPos "sugar6", east, clz] call BIS_fnc_spawnGroup;
	[_sugar6, objNull] spawn epsilon_setSkillAndHe;
	[_sugar6, getmarkerpos "sugar6", 200, 4, [200, 100, 214.449, false, -1], true] call lambs_wp_fnc_taskPatrol;
};

_buawa = [getMarkerPos "buawa", east, ["O_Soldier_TL_F","O_Soldier_GL_F","O_Soldier_AR_F","O_Soldier_LAT_F"]] call BIS_fnc_spawnGroup;
[_buawa, objNull] spawn epsilon_setSkillAndHe;
_buawa setVariable ["lambs_danger_enableGroupReinforce", true, true];
[_buawa, getmarkerpos "buawa", 500, 4, [400, 250, 35, false, -1], true] call lambs_wp_fnc_taskPatrol;

// sugar5 because when they spawn at jungle they can get stuck
_jungle = [getMarkerPos "sugar5", east, ["O_Soldier_TL_F","O_Soldier_GL_F","O_Soldier_AR_F","O_Soldier_LAT_F","O_Soldier_F","O_Soldier_AR_F"]] call BIS_fnc_spawnGroup;
[_jungle, objNull] spawn epsilon_setSkillAndHe;
_jungle setVariable ["lambs_danger_enableGroupReinforce", true, true];
[_jungle, getmarkerpos "jungle", 500, 4, [500, 500, 0, false, -1], true] call lambs_wp_fnc_taskPatrol;
